# Vivaldi

This miniX01 is named after the artist Vivaldi his pieces “The Four Seasons”. It shows the flowers standing through even though the seasons change. Be that flower!

In this miniX01 I have played around with creating things from our daily lives by the help og geometry. The project itself is only made by circles, lines, colours and sure a LOT of triangles! Overall this experience was much more intuitive for me to do - still a bit difficult, but nothing compared to the functions we did in JavaScript. I like that this leaves more room for the creativity.

I like that by the help of coding you’re able to create art, but also come up with solutions for future dilemmas.
For further coding on this project it would be awesome if e.g. the leaves fell off in autumn, it started snowing in winter and that new flowers came in spring.

Please run the code [here](https://lauraAP.gitlab.io/aesthetic-programming/miniX01/)

![](miniX01.png)

Please view the full repository [here](https://gitlab.com/lauraap/aesthetic-programming/-/tree/main/miniX01)

References
<br>
[Picture on pinterest](https://www.pinterest.dk/pin/359865826491361705/)
<br>


