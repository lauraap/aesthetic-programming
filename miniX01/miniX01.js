function setup() {
 // put setup code here
 createCanvas(500,500);
}

function draw() {
  // put drawing code here

  // second quarter
  if(mouseX >= width/4 && mouseX < width/2) {
    background(244,123,32);

  // third quarter
  } else if(mouseX >= width/2 && mouseX < width*0.75) {
    background(62,101,137);
  // fourth quarter
  } else if(mouseX >= width*0.75){
    background(189,238,237);
  // first part
  } else {
    background(138,216,246);
  }

// sun
fill(249, 215, 28);
circle(mouseX, 70, 60);

// ground
line(30, 400, 450, 400);

// flowers
  // purple flower made of triangles
  // flower stem - lines work with x1, y1, x2, y2
  line(50, 400, 50, 270);

  // green leaf
  fill(191, 228, 118);
  triangle(60, 330, 50, 370, 57, 355);

  fill(148, 168, 208);
  // triangle as flower-leaves, x and y numbers for each point of the triangle
  triangle(39, 300, 48 , 278, 46, 303);
  triangle(28, 295, 47 , 275, 35, 300);
  triangle(13, 280, 49 , 270, 22, 292);
  triangle(10, 270, 45, 270, 13, 275);
  triangle(10, 260, 45, 268, 13, 268);
  triangle(10, 256, 44, 264, 14, 240);
  triangle(17, 235, 47, 263, 35, 230);
  triangle(40, 230, 50, 263, 55, 233);
  triangle(59, 233, 53, 264, 62, 233);
  triangle(66, 233, 54 , 268, 75, 245);
  triangle(78, 247, 58 , 268, 80, 258);
  triangle(85, 260, 55 , 272, 88, 264);
  triangle(86, 269, 55 , 271, 82, 278);
  triangle(82, 283, 55 , 274, 76, 288);
  triangle(73, 292, 52 , 274, 60, 300);
  triangle(58, 302, 50 , 274, 54, 302);
  
  fill(252, 169, 133);
  circle(50, 270, 10);

// green flower
line(145, 400, 150, 270);
// leaf
fill(191, 228, 118);
triangle(135, 330, 145, 370, 140, 365);

fill(224, 243, 176);
triangle(139, 300, 148, 278, 146, 303);
triangle(128, 295, 147, 275, 135, 300);
triangle(113, 280, 149, 270, 122, 292);
triangle(110, 270, 145, 270, 113, 275);
triangle(110, 260, 145, 268, 113, 268);
triangle(110, 256, 144, 264, 114, 240);
triangle(117, 235, 147, 263, 135, 230);
triangle(140, 230, 150, 263, 155, 233);
triangle(159, 233, 153, 264, 162, 233);
triangle(166, 233, 154, 268, 175, 245);
triangle(178, 247, 158, 268, 180, 258);
triangle(185, 260, 155, 272, 188, 264);
triangle(186, 269, 155, 271, 182, 278);
triangle(182, 283, 155, 274, 176, 288);
triangle(173, 292, 152, 274, 160, 300);
triangle(158, 302, 150, 274, 154, 302);

fill(255, 250, 129);
circle(150, 270, 10);

// pink flower
line(255, 400, 250, 270);
// leaf
fill(191, 228, 118);
triangle(248, 330, 253, 370, 248, 355);

fill(251, 182, 209);
triangle(239, 300, 248, 278, 246, 303);
triangle(228, 295, 247, 275, 235, 300);
triangle(213, 280, 249, 270, 222, 292);
triangle(210, 270, 245, 270, 213, 275);
triangle(210, 260, 245, 268, 213, 268);
triangle(210, 256, 244, 264, 214, 240);
triangle(217, 235, 247, 263, 235, 230);
triangle(240, 230, 250, 263, 255, 233);
triangle(259, 233, 253, 264, 262, 233);
triangle(266, 233, 254, 268, 275, 245);
triangle(278, 247, 258, 268, 280, 258);
triangle(285, 260, 255, 272, 288, 264);
triangle(286, 269, 255, 271, 282, 278);
triangle(282, 283, 255, 274, 276, 288);
triangle(273, 292, 252, 274, 260, 300);
triangle(258, 302, 250, 274, 254, 302);

fill(252, 169, 133);
circle(250, 270, 10);

// orange flower
line(355, 400, 350, 270);
// leaf
fill(191, 228, 118);
triangle(358, 325, 360, 360, 355, 370);

fill(252, 169, 133);
triangle(339, 300, 348, 278, 346, 303);
triangle(328, 295, 347, 275, 335, 300);
triangle(313, 280, 349, 270, 322, 292);
triangle(310, 270, 345, 270, 313, 275);
triangle(310, 260, 345, 268, 313, 268);
triangle(310, 256, 344, 264, 314, 240);
triangle(317, 235, 347, 263, 335, 230);
triangle(340, 230, 350, 263, 355, 233);
triangle(359, 233, 353, 264, 362, 233);
triangle(366, 233, 354, 268, 375, 245);
triangle(378, 247, 358, 268, 380, 258);
triangle(385, 260, 355, 272, 388, 264);
triangle(386, 269, 355, 271, 382, 278);
triangle(382, 283, 355, 274, 376, 288);
triangle(373, 292, 352, 274, 360, 300);
triangle(358, 302, 350, 274, 354, 302);

fill(72, 181, 163);
circle(350, 270, 10);

// purple flower.2
line(305, 400, 290, 220);
// leaf
fill(191, 228, 118);
triangle(305, 300, 310, 320, 302, 360);

fill(148, 168, 208);
triangle(279, 250, 288, 228, 286, 253);
triangle(268, 245, 287, 225, 275, 250);
triangle(253, 230, 289, 220, 262, 242);
triangle(250, 220, 285, 220, 253, 225);
triangle(250, 210, 285, 218, 253, 218);
triangle(250, 206, 284, 214, 254, 190);
triangle(257, 185, 287, 213, 275, 180);
triangle(280, 180, 290, 213, 295, 183);
triangle(299, 183, 293, 214, 302, 183);
triangle(306, 183, 294, 218, 315, 195);
triangle(318, 197, 298, 218, 320, 208);
triangle(325, 210, 295, 222, 328, 214);
triangle(326, 219, 295, 221, 322, 228);
triangle(322, 233, 295, 224, 316, 238);
triangle(313, 242, 292, 224, 300, 250);
triangle(298, 252, 290, 224, 294, 252);

fill(253, 182, 238);
circle(290, 220, 10);

// orange flower.2
line(60, 400, 70, 220);
// leaf
fill(191, 228, 118);
triangle(75, 300, 75, 320, 63, 360);

fill(252, 169, 133);
triangle(59, 250, 68 , 228, 66, 253);
triangle(48, 245, 67 , 225, 55, 250);
triangle(33, 230, 69 , 220, 42, 242);
triangle(30, 220, 65, 220, 33, 225);
triangle(30, 210, 65, 218, 33, 218);
triangle(30, 206, 64, 214, 34, 190);
triangle(37, 185, 67, 213, 55, 180);
triangle(60, 180, 70, 213, 75, 183);
triangle(79, 183, 73, 214, 82, 183);
triangle(86, 183, 74 , 218, 95, 195);
triangle(98, 197, 78 , 218, 100, 208);
triangle(105, 210, 75 , 222, 108, 214);
triangle(106, 219, 75 , 221, 102, 228);
triangle(102, 233, 75 , 224, 96, 238);
triangle(93, 242, 72 , 224, 80, 250);
triangle(78, 252, 70 , 224, 74, 252);

fill(148, 168, 208);
circle(70, 220, 10);

// yellow flower
line(420, 400, 424, 250);
// leaf
fill(191, 228, 118);
triangle(419, 300, 417, 320, 422, 360);

fill(255, 250, 129);
triangle(414, 280, 423, 258, 421, 283);
triangle(403, 275, 422, 255, 410, 280);
triangle(388, 260, 424, 250, 397, 272);
triangle(385, 250, 420, 250, 388, 255);
triangle(385, 240, 420, 248, 388, 248);
triangle(385, 236, 419, 244, 389, 220);
triangle(392, 215, 422, 243, 410, 210);
triangle(415, 210, 425, 243, 430, 213);
triangle(433, 213, 427, 244, 436, 213);
triangle(440, 213, 428, 248, 449, 225);
triangle(452, 225, 432, 248, 454, 238);
triangle(461, 238, 431, 253, 464, 245);
triangle(465, 252, 434, 254, 461, 261);
triangle(460, 268, 433, 257, 454, 272);
triangle(452, 275, 428, 256, 437, 283);
triangle(426, 285, 426, 254, 432, 284);

fill(253, 182, 238);
circle(425, 250, 10);

fill(0);
text('drag the mouse from one side to the other and change the seasons!',50, 420);
}
