let positions;
let cryingEmoji;
let outsideScreenText = " you can always stop by going outside the screen."; // content for text animation 
let textStartPoint = 0; // x start point for the array
let textRepetition = 10; // how many times the text should be written
let font;
let showFullWebcamCapture = false // creating a boolean for the webcam capture

function preload() {
  cryingEmoji = loadImage('CryingEmoji.webp'); // get crying emoji
  font = loadFont('Inconsolata/Inconsolata.otf'); // get a new font
} 

let faceTracker;
function setupFaceTracker() {
  faceTracker = new clm.tracker(); // tracks a face and outputs the coordinate positions
  faceTracker.init(pModel);
  faceTracker.start(videoCapture.elt);
}

let videoCapture;
function createVideoCapture() {
  videoCapture = createCapture(VIDEO);// creates video element that captures the video stream from the webcam
  videoCapture.size(640, 480);
  videoCapture.position(0, 0);
  videoCapture.hide();// DOM element - we want to see the manipulated version first
}

let buttonLoveThis;
let buttonHateThis;
let buttonOops;

function setupLoveThisButton(){
  buttonLoveThis = createButton('I LOVE THIS!');// creates a <button></button> element saying "I LOVE THIS"
  buttonLoveThis.position(160, 400);
  buttonLoveThis.style("padding", "5px");
  buttonLoveThis.style("color", "orange");
  buttonLoveThis.style("outline", "none");
  // adding an eventlistener to the button using mousePressed
  buttonLoveThis.mousePressed(revealPersonBehind);
}

function setupHateThisButton(){
  buttonHateThis = createButton('I HATE THIS!');// creates a <button></button> element saying "I HATE THIS"
  buttonHateThis.position(400, 400);
  buttonHateThis.style("padding", "5px");
  buttonHateThis.style("color", "orange");
  buttonHateThis.style("outline", "none");
  // adding an eventlistener to the button using mousePressed - higher order function
  buttonHateThis.mousePressed(revealPersonBehind);
}

function revealPersonBehind(){
  showFullWebcamCapture = !showFullWebcamCapture // flipped boolean - change the value of the boolean to the opposite
}

function setupOopsButton(){
  buttonOops = createButton('Oops! Looks like we got data about you. Thank you for revealing yourself:)');
  // creates a <button></button> element saying "Oops!..."
  buttonOops.position(100, 400);
  buttonOops.style("padding", "5px");
  buttonOops.style("color", "red");
  buttonOops.style("outline", "none");
}

function errorBox(x, y){ // box representing the user did something wrong
  // elements on top of each other to get the error-box vibe from Microsoft
  fill(0, 0, 190);
  rect(x, y, 250, 150);

  fill(255, 255, 230);
  rect(x+5, y+25, 240, 120);

  push();
  fill(230, 0, 0);
  noStroke();
  ellipse(285, 200, 30, 30);
  pop();
  fill(255);
  textSize(20);
  text('X', 284, 200);

  textSize(20);
  text('E R R O R', 360, 160);
  fill(0)
  textSize(18)
  text('Why did you leave????', 380, 240);
}

function setup() {
  createCanvas(640, 480);
  createVideoCapture();
  setupFaceTracker();
  setupLoveThisButton();
  setupHateThisButton();
  setupOopsButton();
}

function draw() {
 clear(); // clears the pixels on the canvas - making every pixel transparent
 background(255); // sets the background to white
 
 function drawFace(){
  //face
  fill(255); // sets the fill of the face-vertex to white - float effect on top of white background

    positions = faceTracker.getCurrentPosition(); // assigns the positions of the faceTracker to the variable positions

    if (positions.length){ // checks to see if there are any points of the faceTracker
    beginShape(); // the start of creating a more complex/unique form - by the help of vertices defined here:
    vertex(0,0);
    vertex(0, width);
    vertex(width, height);
    vertex(0, height);
    beginContour(); // the start of recording the vertices for the outline of the face

    for (var i=0; i<19; i++) {
      vertex(positions[i][0], positions[i][1]); // creates the 'downpart' of the face
    } 
    vertex(positions[22] [0], positions [22] [1]); // creates the rest of the face
    vertex(positions[21] [0], positions [21] [1]); // creates the rest of the face
    vertex(positions[20] [0], positions [20] [1]); // creates the rest of the face
    vertex(positions[19] [0], positions [19] [1]); // creates the rest of the face
    endContour(); // stops recording the vertices for the outline of the face
    endShape(CLOSE); // end of the form

    beginShape(); // right eye
    vertex(positions[23] [0], positions [23] [1]); 
    vertex(positions[63] [0], positions [63] [1]); 
    vertex(positions[24] [0], positions [24] [1]);
    vertex(positions[64] [0], positions [64] [1]);
    vertex(positions[25] [0], positions [25] [1]);
    vertex(positions[65] [0], positions [65] [1]);
    vertex(positions[26] [0], positions [26] [1]);
    vertex(positions[66] [0], positions [66] [1]);
    endShape(CLOSE);
    push();// using push and pop to only fill the ellipse with black
    fill(0)
    ellipse(positions[27] [0], positions [27] [1], 10, 10);// creates the pupil
    pop();

    beginShape(); // left eye
    vertex(positions[30] [0], positions [30] [1]); 
    vertex(positions[68] [0], positions [68] [1]); 
    vertex(positions[29] [0], positions [29] [1]);
    vertex(positions[67] [0], positions [67] [1]);
    vertex(positions[28] [0], positions [28] [1]);
    vertex(positions[70] [0], positions [70] [1]);
    vertex(positions[31] [0], positions [31] [1]);
    vertex(positions[69] [0], positions [69] [1]);
    endShape(CLOSE);
    push();// using push and pop to only fill the ellipse with black
    fill(0)
    ellipse(positions[32] [0], positions [32] [1], 10, 10);// creates the pupil
    pop();

    beginShape(); // mouth
    for (var i=44; i<56; i++) {
      vertex(positions[i][0], positions[i][1]); // creates the outline of the mouth
    } 
    vertex(positions[44] [0], positions [44] [1]); 
    vertex(positions[56] [0], positions [56] [1]); 
    vertex(positions[57] [0], positions [57] [1]);
    vertex(positions[58] [0], positions [58] [1]);
    vertex(positions[50] [0], positions [50] [1]);
    vertex(positions[59] [0], positions [59] [1]);
    vertex(positions[60] [0], positions [60] [1]);
    vertex(positions[61] [0], positions [61] [1]);
    endShape(CLOSE);

    // text in buttom
    fill(0, 0, 255);
    textSize(14);
    textAlign(CENTER, CENTER); // allignment of text 
    text(outsideScreenText.repeat(textRepetition), textStartPoint, 470); // repetition of array

    // it continues to write the array outSideScreenText
    textStartPoint += 2; // speed of animation
    // the position of the array outSideScreenText is reset if the TextStartPoint is beyond the width
    if (textStartPoint > width){
      textStartPoint = -textWidth(outsideScreenText); // from the left to the right
    }

    fill(0);
    textFont(font);
    textSize(20);
    text('Here you are allowed to be anonymus', width / 2, 100);

    buttonLoveThis.show();// showing the "I LOVE THIS" button on the page
    buttonHateThis.show();// showing the "I HATE THIS" button on the page
    buttonOops.hide();// hiding the "Oops!..." button on the page

  } else { //if there is no face captured on the screen
    background(150, 150, 150); // the background turns grey
    image(cryingEmoji, 100, 200);// the loaded image of a crying emoji appears
    cryingEmoji.resize(200, 200);// resizing the image, since it was way to big
    errorBox(250, 150);// the errorBox function is called
    buttonLoveThis.hide();// hiding the "I LOVE THIS" button on the page
    buttonHateThis.hide();// hiding the "I HATE THIS" button on the page
    buttonOops.hide();// hiding the "Oops!..." button on the page
  }
}

// if statement to decide what happens when the boolean is false and when it's true
  if (!showFullWebcamCapture){ //if showFullWebcamCapture is false the do the if statement
  drawFace();
  } else { //if showFullWebcamCapture is true show the original webcam capture
    image(videoCapture, 0, 0, 640, 480);
    buttonLoveThis.hide();// hiding the "I LOVE THIS" button on the page
    buttonHateThis.hide();// hiding the "I HATE THIS" button on the page
    buttonOops.show();// showing the "Oops!..." button on the page
  }
}
