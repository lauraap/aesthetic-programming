# An.onY.mo.US

Please run the code [here](https://LauraAP.gitlab.io/aesthetic-programming/miniX04/) (best in chrome!)

Please view the code [here](https://gitlab.com/lauraap/aesthetic-programming/-/blob/main/miniX04/miniX04.js?ref_type=heads)

In this project an attempt to be anonymous has set its feet on the ground of our digital world. Where we give our data as free food to hungry money-focused-companies. This project was created to put into light how NOTHING on the internet is for free - even though it might seem like it. Here you are allowed to be anonymous but if you press any button or disappear from the screen 'errors' will occur. You will fail at being anonymous even though that's what the whole concept of this webpage is about.

This miniX04 is meant as an approach to consideration about what information we give our computers. Because no matter what, the companies out there would like to get some money based on our data. When making this miniX I was really inspired by a VRPO documentary on the Big datarobbery. This documentary has an example of the Google Nest where a hidden microphone is attached to it without the users knowing. And when confronting Google with it, they said they didn't know anything about it. I tried doing this in my miniX by telling the user, that this is a space for them to be 100% anonymous, but then when they interact with the website data is stealed from them anyway. It's a representation of how companies promise something and do something else. 

Through this project I have gotten more concerned about big companies out in the world and how focused they are on my data. It's important to consider which data we allow other people to have, because they want to capture (it) all!

But what's wrong with our data being captured? Is there a problem with the datafication? Personally I think it's creepy how everything in our lives suddenly has become data for someone and transferred into information for companies to earn revenue from. I question whether or not this is healthy for the world we live in. Yes, ChatGPT is a very helpful tool, but how helpful is it actually? If we learn through AI how much do we actually learn? And will the computer someday rule the world? That's some of my concerns about our society - Another reason for my take on this miniX04. You are most likely never anonymous and never will be when interacting with computers.

![](Anonymous.mov)

_**The code explained**_
For this miniX04 I have played around with some new elements in programming. Here I will explain the elements createCapture(VIDEO), clmtracker, for loop. DOM elements,  and a piece of my code using a flipped boolean.

**createCapture(VIDEO)** creates a video element which captures the video stream from the webcam. I used this for making the face on the screen and also for revealing the person behind the screen (you).
Along with this createCapture(VIDEO) this code also uses a **clmtracker** that tracks a face and outputs the coordinate positions of the face model as an array. This is also shown in this illustration where I also explain the **for loop** in my code.
![](faceTracker.png)

**DOM elements** is very useful when wanting to create something in a specific way. In this way we are able to manipulate the styling of HTML elements. I used it in my code to change the style and position of the buttons I made. But it's also a way of processing the actions of the user. An example is in my code where my button "buttonLoveThis" gets the event listener called mousePressed which has the callback function revealPersonBehind as the argument. In this way the input comes from when the user press the mouse, the processing is through the p5 library that knows the action of a mouse being pressed and the output is in the revealPersonBehind function.

The revealPersonBehind function consists of the boolean showFullWebcamCapture which in the top of the code is declared as false. More concrete in the revealPersonBehind function I create a **flipped boolean** which changes the value of the boolean to the opposite - you can go back and forth between true and false. In the end of my code I create an if statement that decides what happens when the boolean is false and when it's true. Because the boolean is false by default I use the ! to negate the value of showFullWebcamCapture - otherwise the if-statement will not be executed. So overall the if statement states that if the showFullWebcamCapture boolean is false the drawFace function will be executed and if not it will show what the webcam is actually capturing.

**References used for this miniX**
<br>
[VRPO documentary](https://youtu.be/hIXhnWUmMvw.)
<br>
["Aesthetic programming" chapter 4](https://aesthetic-programming.net/pages/4-data-capture.html)
<br>
[Transmediale "Capture all](https://transmediale.de/content/call-for-works-2015)