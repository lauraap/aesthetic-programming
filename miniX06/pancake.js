class Pancake {
  constructor(name, img) {
    this.name = name;
    this.pos = new createVector(random(100, windowWidth), random(320, windowHeight-100));
    this.size = {w: 60, h: 60};
    this.image = img;
  }

  show(){
    push();
    imageMode(CENTER);
    image(this.image, this.pos.x, this.pos.y, this.size.w, this.size.h);
    pop();
    }
}