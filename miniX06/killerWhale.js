class KillerWhale {
  constructor() {
  this.speed = floor(random(1, 4));
  this.pos = new createVector(windowWidth + 5, random(320, windowHeight-50));
  this.size = floor(random(100, 400));
  }
  move() {
    this.pos.x -= this.speed;
  }
  show() {
    push();
    imageMode(CENTER);
    image(kwBackIMG, this.pos.x, this.pos.y, this.size, this.size);
    pop();
  }
}