// background colors
let c1;
let c2;
// images
let logo, icebergIMG, penguinIMG, pancakePenguinIMG, kwBackIMG, flourIMG, eggIMG, sugarIMG, milkIMG, cardamomIMG, butterIMG, panIMG, gameOverIMG;
// killer whales
let killerWhaleArray = [];
let min_killerWhale = 6;
// pancake
let ingredients = [];
let min_ingredient = 1;
let total = 7;
let ingredientsMade = false;
// gamestate
let stage = "startScreen";
let collectedIngredients = 0, lives = 3;
let penguin;
let keyColor = 70;



function preload() { // preload is called before  the program is ran - loading image assets
  logo = loadImage('logo.png');
  icebergIMG = loadImage('iceberg.png');
  penguinIMG = loadImage('penguin.png');
  pancakePenguinIMG = loadImage('pancakePenguin.png');
  kwBackIMG = loadImage('killerWhaleBack.png');
  flourIMG = loadImage('flour.png');
  eggIMG = loadImage('egg.png');
  sugarIMG = loadImage('sugar.png');
  milkIMG = loadImage('milk.png');
  cardamomIMG = loadImage('cardamom.png');
  butterIMG = loadImage('butter.png');
  panIMG = loadImage('pan.png');
  gameOverIMG = loadImage('gameOver.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  penguin = new Penguin(); // creating a new penguin object called penguin
}
 
 function draw() {
  if (stage == "startScreen"){ // when starting...
    drawBackground(); // draw the gradient background
     
    image(icebergIMG, 0, 20, windowWidth, 800);
    image(logo, 500, 50, 400, 400);

    textSize(19);
    fill(keyColor, 255);
    textAlign(CENTER);
    text('use the ARROW KEYS to avoid the killer whales and collect pancake ingredients', 550, 350, 300)
    fill(keyColor, 160);
    textSize(17);
    text('press ENTER to start', 700, 450);
  }

  if (stage == "gamePlay"){ // when playing...
    drawBackground();
    image(icebergIMG, 0, 20, windowWidth, 800);
    image(logo, 625, -30, 200, 200);

    penguin.show(); // using method 'show' on the object penguin
    penguin.move(); // using method 'move' on the object penguin

    // functions connected to the object Pancake
    showIngredient();
    checkIngredientCollected();
    if(!ingredients.length) { // will evaluate to true if the array is ampty
    stage = "win"; // if all ingredients are collected turn to win stage
  }
    // functions connected to the object KillerWhale
    checkKillerWhaleOffscreen();
    checkKillerWhaleNum();
    showKillerWhales();
    checkKillerWhaleEat();

    displayScore();
    }

    if (stage == "win"){ // when winning...
    image(pancakePenguinIMG, 480, 100);
    }

    if (stage == "gameOver") { // when loosing...
      background(0);
      image(gameOverIMG, 500, 100);
    }
 }

 function keyPressed(){ // p5 function to check if a key is pressed
  if (keyCode === ENTER) {
    if (stage === "startScreen" || stage === "gameOver") {
      stage = "gamePlay";
      setUpIngredients(); // every time the game is played the ingredients should be refreshed
      collectedIngredients = 0;
      lives = 3; // every time the game is played the player has three lives
    } else if (stage == "win") // return to start when winning
      stage = "startScreen"
  }
}

function drawBackground(){ 
  // Drawing the gradient background
  c1 = color(255); // white color
  c2 = color(63, 191, 191); // turqouise color
    
  for(let y=0; y<windowHeight; y++){ // iterates over the height of the canvas - draws lines vertically to create gradience
    n = map(y, 0, windowHeight, 0, 1); // the mapped value 'n' will be used to interpolate between the two colors
    let newc = lerpColor(c1, c2, n); // 'lerpColor' is interpolating between two colors - blends two colors
    stroke(newc); // each line drawn has the color that corresponds to the gradient between c1 and c2
    line(0, y, windowWidth, y); // horisontal line at the current y position - spanning the window width
  }
 }

// creating the ingredients
 function setUpIngredients() {
  if (!ingredientsMade) { // if the ingrediants have not been initialized yet
    ingredients = []; // reset the array of the ingredients
    // initialize the pancake ingredients and store them in the ingredients array
    let ingredientImages = [flourIMG, eggIMG, sugarIMG, milkIMG, cardamomIMG, butterIMG, panIMG];
    let ingredientNames = ["Flour", "Egg", "Sugar", "Milk", "Cardamom", "Butter", "Pan"];
    for (let i = 0; i < ingredientImages.length; i++) {
      console.log("i loop");
      ingredients.push(new Pancake(ingredientNames[i], ingredientImages[i]));
      print(i);
      console.log(ingredients);
    }
  }
 }

function showIngredient(){ // show the current ingredient on the screen 
  if (ingredients.length > 0) { // checks if the array has any elements
    ingredients[0].show(); // if there is anything in the array show the first element
  }
}

function checkIngredientCollected(){ // checking if the penguin has collected an ingredient
  if (ingredients.length > 0) { // checks if the array has any elements
    let ing = ingredients[0];
    let d = dist(penguin.pos.x, penguin.pos.y, ing.pos.x, ing.pos.y);
    if (d < penguin.size.h/2){ // if there are any elements in the array calculate the distance between penguin and ingredient
      ingredients.shift(); // if the distance is less than the height of the penguin remove the first ingredient from the array
      collectedIngredients++; // and increase the ingredient counter by 1
    }
  } 
}

// creating the killerWhales

function showKillerWhales(){ // show and move the current killer whale on the screen
  for (let i = 0; i < killerWhaleArray.length; i++) { // iterates through the killer whale array with KillerWhale objects
    killerWhaleArray[i].show(); // for each killer whale show
    killerWhaleArray[i].move(); // for each killer whale move
  }
}

function checkKillerWhaleNum(){ // checks the number of killer whales currently present in the game
  if (killerWhaleArray.length < min_killerWhale) { // if the number of killer whales is less than the minimun required number
    killerWhaleArray.push(new KillerWhale()); // add a new object of KillerWhale to the array - via 'push'
  }
}

function checkKillerWhaleOffscreen(){ // checks if any of the killer whales have moved outside the screen
  for (let i = killerWhaleArray.length - 1; i >= 0; i--) { // iterates through the array but from the back
    let = currentKW = killerWhaleArray[i];
    if ((currentKW.pos.x + currentKW.size/2) < 0) { // for each killer whale calculate the pos.x and size/2
      killerWhaleArray.splice(i, 1); // if the pos is less than 0 remove the killer whale from the array
    }
  }
}

function checkKillerWhaleEat(){ // checks if penguin and killer whale has collided
  for (let i = 0; i < killerWhaleArray.length; i++){ // iterates through the killerWhaleArray
    let d = dist(penguin.pos.x, penguin.pos.y, killerWhaleArray[i].pos.x, killerWhaleArray[i].pos.y) // calculating the distance between p and kw
    if (d < penguin.size.h/2) { // if the distance is less than half the height of the penguin
      killerWhaleArray.splice(i, 1); // remove this killer whale from the array
      lives--; // decrements the lives by 1
      if (lives <= 0) { // if the player has run out of lives
        stage = "gameOver"; // stage turns to "gameOver" stage
      }
    }
  }
}

// keep track of lives and ingredients
function displayScore() {
  fill(0,0,255);
  textSize(20);
  text('Ingredients collected: '+collectedIngredients + "/" + total, 118, 50);
  text('Lifes left: '+lives, 55, 75);
}