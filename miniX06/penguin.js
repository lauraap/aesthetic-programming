class Penguin {
  constructor(){
    this.pos = new createVector(1000, 350);
    this.size = {w: 100, h: 100};
  }

  show() {
    push();
    imageMode(CENTER);
    image(penguinIMG, constrain(this.pos.x, 50, windowWidth-50), constrain(this.pos.y, 320, windowHeight-50), this.size.w, this.size.h);
    // constrain is to make sure that the penguin stays inside of the website frame
    pop();
  }

 move() {
    if (keyIsDown(UP_ARROW)) {
      this.pos.y -= 7;
    } else if (keyIsDown(DOWN_ARROW)) {
      this.pos.y += 7;
    } else if (keyIsDown(LEFT_ARROW)) {
      this.pos.x -= 7;
    } else if (keyIsDown(RIGHT_ARROW)) {
      this.pos.x += 7;
    }
  }
}