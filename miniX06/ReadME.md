# **Pancake Penguin** 🥞🐧❄️

Please run the code [here](https://LauraAP.gitlab.io/aesthetic-programming/miniX06/)

Please view the full repository [here](https://gitlab.com/lauraap/aesthetic-programming/-/tree/main/miniX06)

![](PP.mov)

## ReadME
In Pancake Penguin three objects appear:
1) `Penguin`
2) `Pancake`
3) `KillerWhale`

Each object has its own set of methods. However they look much alike since they all have a method `show` and both `Penguin` and `KillerWhale` also contains the method `move`. This means that when a new instance of the object `Penguin`is made, and later added the methods to it, it will show and move in the way the methods are made in the object. In the following a more in depth description of the objects will occur:
`Penguin`
If an instance of the object `Penguin` is made `this.__`will tell what messages the object of this moment is receiving. In this case the object will receive a message about the position and the size. Here I have used `createVector()` to easily represent the position of my penguin. Later in the `show`method this position vector is used to determine where to display the penguinIMG on the canvas. In this method I have used constrains to secure that the penguin is staying inside of the lake.
`Pancake`
The `Pancake` object is the goal of this game. The goal is to collect the 7 ingredients for the pancakes to be made. Normally my name for this object was Ingredients, but the p5.js was somehow not happy with that - so I changed it to Pancake, and actually also like that the instances of the `Pancake`object are ingredients. Just like making pancakes in real life. Since the ingredients don't have to move the only method for this object is `show`. However the random function is used here to place the ingredients of the pancake randomly on the screen. This creates a sort of obstacle when collecting the ingredients.
`KillerWhale`
However, the main obstacle of this game are the killer whales. Just like the `Penguin`object an instance of this object can have two methods. The `show`method is pretty much the same as with the `Penguin`object, but that is not the case with the `move`method. Here the killer whale is set to have a speed and additionally to move horizontally from the right to the left of the screen. For each killer whale created the speed is random between 1 and 4, which creates some variation to the game.

In my code I start by making one instance of the class/object `Penguin`. 
Later I assign the methods `show`and `move`to it.

The ingredients of the pancake is made through the functions `setUpIngredients()`, `showIngredient()`and `checkIngredientCollected()`. In the code these functions are explained more into depth.

Killer whales are added to the game through four functions called `checkKillerWhaleOffscreen();`, `checkKillerWhaleNum();`, `showKillerWhales();`, `checkKillerWhaleEat();`. These are explained in my code.

In my game I also operate with different stages. This was a way of creating more structure when programming the game but also to create a more realistic game - games don't just start when put on the screen. The game has four stages; `startScreen`, `gamePlay`, `win`, `gameOver`. Both `startScreen`and `gamePlay`one will always see when playing, but `win`and `gameOver`will only appear if winning or loosing. These stages are controlled by the p5 function called `keyPressed();` with the `keyCode === ENTER`.


In the Aesthetic programming book there is a focus on "How computational objects model the world and what this suggest in terms of an understanding of hidden layers of operation and meaning." Pancake Penguin is not a game based on something that could actually happen in the real world (or some might say that it is possible for a penguin to be eaten by a killer whale - but probably not in the search of pancake ingredients). So this penguin-hunt for pancake ingredients is a very specific situation of something unrealistic. I think this is the case for many computer games. They model the world through and abstraction of it. It is not necessarily something realistic that takes place but it is still modeling the world through the eyes of the creator of the game. The objects put into a game are designed with certain assumptions, biases and worldviews and to create better OOP we must know about this. It is important that we focus on the purpose of the different objects in a game.

### **References**
[Aesthetic Programming chapter 6](https://aesthetic-programming.net/pages/6-object-abstraction.html)
<br>
[Aesthetic Programming Showcase; Trine](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX7/Readme.md)
<br>
Helena from the class made the penguin
<br>
Got help from Nikolaj from DD LAB
<br>
ChatGPT