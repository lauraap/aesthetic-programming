let x = 700;
let y = 350;
let rnd;
let emojiArray = ['☀️', '🌻', '💛', '🥧', '🐝', '🍯'];

function hexagon(transX, transY, n, o) { // transX and transY are saying that the x and y are translated
  // n = size of the hexagon
  // o = opacity 
  stroke(255);
  strokeWeight(5);
  fill(255, 200, 100, o);
  push();
  translate(transX, transY);
  beginShape();
  vertex(-75*n, -130*n);
  vertex(75*n, -130*n);
  vertex(150*n, 0*n);
  vertex(75*n, 130*n);
  vertex(-75*n, 130*n);
  vertex(-150*n, 0*n);
  endShape(CLOSE); 
  pop();
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255, 150, 0); // orange background color

  // creating honeycomb background
  // this nested for loop creates hexagons in columns and rows
  for (let i = 0; i < 12; i++) { // creating 12 colums
    for (let j = 0; j < 8; j++) { // creating 8 rows. each iteration represents a row within a column.
      if (i %2 ==0) { // the modulo operator checks if 'i' is even or odd
        hexagon(i * 130, j * 130 + 35, 0.4, random(0, 500)); // Adjust y-coordinate by 35 pixels
      } else {
        hexagon(i * 130, j * 130, 0.4, random(0, 500)); // Keep the first row unchanged
      }
      // the if statement makes the columns staggered
      // if 'i' is even the transY parameter of the 'heaxgon()' function is adjusted by 35 pixels
      // 'i' * 130 tells the hexagon x-coordinate to start at 130 and mulitply with 'i'
      // 'j' * 130 tells the hexagon y-coordinate to start at 130 and mulitply with 'j'
      // the multiplication is due to the changing value of 'i' and 'j'
      // 0.4 is the size of the hexagons
      // random opacity
    }
  }

  rnd = random(1, 2) // assigns the random function to pick a number between 1 and 2
  hexagon(x, y, rnd); // draws hexagon at x and y with a random size between 1 and 2

}
 
function draw() {
  x += random(-5, 5); // how much space between the elements of the array
  y += random(-5, 5); // how much space between the elements of the array
  if (dist(700, 350, x, y) > 125*rnd) { // to make sure the text stays inside the hexagon
    // dist calculates the difference between two points
    // if the distance between the points given is greater than 125*rnd go back to the start
    // multiply with rnd so it follows the random size of the big hexagon
    x = 700;
    y = 350;
}

  // Draw the text.
  text(random(emojiArray), x, y);
}