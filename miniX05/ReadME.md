# **Auto.Honeycomb.generator**
☀️ 🌻 💛 🥧 🐝 🍯

Please run the code [here](https://LauraAP.gitlab.io/aesthetic-programming/miniX05/)

Please view the full repository [here](https://gitlab.com/lauraAP/aesthetic-programming/)

![](honeycomb.mov)

This miniX explores the term auto-generation - a term invented by mathematician Alan Turing. Turing was concerned with solving a formal logic problem: What math problems can solved by following simple logical rules? So how much math can we do just by following simple rules. To get an understanding of how algorithms work this way Alan Turing created the Turing machine.

The Turing machine is able to make computations and check if an input is accepted or not accepted under given constraints. The concept/structure of the Turing machines makes arithmetic computations, changing values and checking inputs possible. With these building blocks/rules we can build more complicated Turing machines/programs. Turing machines can be deterministic or not deterministic, meaning that non-deterministic Turing machines might produce different results from the same input. Program is non-deterministic, since it takes no input and produces different results.

In my miniX I set two rules before I started coding.
<br>
	Rule 1) Every time the function draw() is run, an element from an array should move randomly.
<br>
	Rule 2) Every time elements from an array goes outside an object, go back to the start-point
<br>
The funny thing about this is to watch the website being refreshed. Because when refreshed one sees that even though the it is the same set of instructions that run through the code, the way the website performs is differently from time to time. It actually acts just as Christopher Strachey puts it: "simple rules can generate diverse and unexpected results". As mentioned before, the program is non-deterministic.
Because this code is exclusively consisting of a basic set of rules and not on any input from the user, they produce this emergent behavior. The code does not depend on individual parts being activated, but instead on their relationship.

The reason for this emergent behavior is thanks to the function `random()`. This function is able to return a number or a random element from an array (in this case: an element from the array `emojiArray`). So every time the frame is drawn the code draws a random emoji from the array. Looking at the specific lines of code where this is acted out: 
`x += random(-5, 5);`
`y += random(-5, 5);`
one might look not only at the function `random()` but also the `+=`. What this does is it takes e.g.`x` and says x = x + random(-10, 10); So every time the function`draw()` is run the x is assigned a new value. Additionally if we look at the syntax for `text` where the parameters are a string, an x-coordinate and an y-coordinate. When these parameters become random this randomness of where the text is drawn is much like drawing a doodle with a pencil. 

I got a bit carried away with the randomness but as is written in 10_PRINT "randomness is necessary for any statistical endeavor, any simulation that involves unknown variables" so I think of it as a good thing.
Therefore this code also contains a big hexagon where the size changes from every time the code is run, and the background actually also uses the random function to change the opacity of the hexagons. This was a way of creating this honeycomb effect.

To make the honeycomb background I used a nested for loop (a loop within a loop). This was a way of creating rows and columns of hexagons. Inside the nested for loop is an if statement that checks if `i` is an even or an odd number. If it is an even number the `transY` parameter of the `hexagon()` function is adjusted by adding 35 pixels. This creates a staggered effect.

In a way bees are also auto-generators. They have the Queen bee witch sets some rules for them and then they just have to follow and create honey, although the honey might not be good every time.

The creation of my miniX05 has helped me understand the idea of "auto-generator". It has made me realise that the programmer is able to create a lot of things by just implementing a few rules. In fact this is the most simple code I have made through these miniX's but a lot still happens - and not through user input. In a way I also find this pseudorandomness magical. Just making rules for something to become so random that it creates art - magic!
However I also think it is interesting to consider who is the actual artist of this miniX05? Yes, I am in charge of putting this code together, but the things I used (such as emojis) are not my creation. I created new art but it was by the help of already created art.

### **References**
[Aesthetic Programming: A Handbook of Software Studies, chapter 5 "Auto-generator"](https://aesthetic-programming.net/pages/5-auto-generator.html)
<br>
[10_PRINT, chapter 8, "Randomness"](https://10print.org)
<br>
[p5.js.org reference (especially the property 'random')](https://p5js.org/reference/)
<br>
[The Coding Train "Arrays and Loops"](https://www.youtube.com/watch?v=RXWO3mFuW-I&t=222s)
<br>
[Hexagon code](https://editor.p5js.org/jenagosta/sketches/Sy5wzBblg)
<br>
ChatGPT