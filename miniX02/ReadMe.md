# HAPPY.SAD.EMOJI

In this miniX02 I tried out the translate function in order to play with the displacement of certain objects - in this case the ellipse. To help me getting the ellipse to move around I used the function called rotate (funny enough). This function rotates the created ellipse by the amount specified by the angle parameter. Here I created a global variable called deg (as in degrees) and assigned the value 12 to it. This ment that the object (ellipse) would rotate 12 radians every time the code run. However to see that the ellipse was moving would be difficult because of the 2D, so I also assigned +3 to the variable deg, so that every time the code run it would rotate 12 but add another 3.

To create my other emoji I used an ellipse but also practiced the concept of vertex. Here I used beginShape and endShape where you are allowed to place as many vertex as you want. For me vertex was the perfect drawing material for creating something very specific. I used points to tell the vertex from where to start and where to stop. Somehow I also used the parameter (CLOSED) to tell the vertex to make it an actual shape. For the tongue and the "middle" of the eye, I used curvedVertex. The combination of vertex and curves allowed me to make the exact shape of the emoji tongue and also the kind of curves that attend in the eyes. In addition to this I found it funny to play around with the rgb color to get the color I wanted - especially the tongue. 

Because I was making two emojis and I didn’t want them both to be rotating, I practiced the push() and pop() functions. These were able to help me making only the ellipse turning. In that way I could create a non-rotating other emoji.

The deeper meaning of the first emoji were the feeling of being happy but still having s-/bad days - therefore I also found the name HAPPY.SAD.EMOJI good. When everything is good but you still feel sad. When you're supposed to be grateful, happy &/or having fun but you feel the pressure to do all these things, so you instead turn sad - the original smile now faces down.
The second emoji was just me trying to recreate one of my favourite emojis, really trying to play around with geometry. And in addition to the meaning of the first emoji the one I use, when I'm happy;)

Please run the code [here](https://LauraAP.gitlab.io/aesthetic-programming/miniX02/)

![](emoji.mov)

My gitlab [here](https://gitlab.com/lauraAP/aesthetic-programming)