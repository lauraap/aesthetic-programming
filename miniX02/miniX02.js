let deg = 12;
var clr;

function setup() {
 // put setup code here
 createCanvas(1000, 1000);

}

function draw() {
  // put drawing code here

  // emoji.1
  push(); 
  translate(150, 150);
  rotate(radians(deg));
  // happy/sad emoji
  fill(255, 222, 52);
  stroke(255, 186, 0);
  ellipse(5, 5, 200); // face
  noStroke();
  fill(0)
  ellipse(5, 10, 180, 150); // mouth
  fill(255, 222, 52);
  ellipse(5, 0, 180, 150); // mouth cut
  fill(0);
  ellipse(50, -20, 45); // left eye
  ellipse(-40, -20, 45); // right eye
  fill(255, 222, 52);
  ellipse(50, -5, 45); // left eye cut
  ellipse(-40, -5, 45); // right eye cut
  pop();

  deg = deg+=3;

  // emoji.2
  fill(255, 222, 52);
  noStroke();
  ellipse(500, 150, 200); // face
  fill(205, 133, 63);
  ellipse(500, 175, 120, 90); // mouth
  fill(255, 222, 52);
  ellipse(500, 140, 160, 70);

  // LEFT EYE
  // left eye points
  point(455, 100);
  point(490, 120);
  point(455, 140);
  point(480, 120);

  stroke(0);
  fill(0);

  beginShape(); // left eye shape
  // vertex(x, y, [z])
  vertex(455, 100);
  vertex(490, 120);
  curveVertex(455, 140);
  curveVertex(480, 120);
  endShape(CLOSE);

  // RIGHT EYE
  // right eye points
  point(540, 100);
  point(515, 120);
  point(540, 140);
  point(505, 120);

  beginShape(); // right eye shape
  vertex(540, 100);
  vertex(515, 120);
  curveVertex(540, 140);
  curveVertex(505, 120);
  endShape(CLOSE);

  // TONGUE
  //stroke(255, 0, 0);
  //strokeWeight(4);
  point(480, 190);
  point(470, 230);
  point(485, 260);
  point(515, 240);
  point(520, 190);
  //strokeWeight(1);

  stroke(255, 50, 203);
  fill(255, 100, 203);
  beginShape();
  curveVertex(480, 190);
  curveVertex(470, 230);
  curveVertex(485, 260);
  curveVertex(515, 240);
  curveVertex(520, 190);
  endShape(CLOSE);

  // line on tongue
  stroke(255, 0, 280);
  point(500, 190);
  point(490, 210);

  beginShape();
  curveVertex(500, 190);
  curveVertex(493, 230);
  endShape(CLOSE);
   
}
