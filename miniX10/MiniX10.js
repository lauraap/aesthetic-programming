// Declaring all global variables:
let buttons = [];
let buttonsName = ["PICK LAURA", "PICK LASSE", "PICK OLIVE", "PICK SIGNE"];
let boxImages = [];
let boxObjects = [];
let colors = ["Purple", "Blue", "Green", "Orange"];
let min_BoxObjects = 7;

let audioFiles = [];
let buttonSounds = [];
let buttonLabels = ["ABOUT LAURA", "ABOUT LASSE", "ABOUT OLIVE", "ABOUT SIGNE"];

let myFont;
let screen = 0;

let backgroundMusic;

let selectedCharacter;

/*let lauraScore = 0;
let lasseScore = 0;
let oliveScore = 0;
let signeScore = 0;*/

let buttonsCreated = false;  // Track button creation

//------------------------ PRELOAD-------------------------
//load images and sound
function preload() {

  //load charecters images
  oliGif = loadImage("gif/oli.gif");
  lasseGif = loadImage("gif/lasse.gif");
  lauraGif = loadImage("gif/laura.gif");
  signeGif = loadImage("gif/signe.gif");

  oliBox = loadImage("gif/oliBox.gif");
  lasseBox = loadImage("gif/lasseBox.gif");
  lauraBox = loadImage("gif/lauraBox.gif");
  signeBox = loadImage("gif/signeBox.gif");

  //load assemblyline and boxes
  
  assemblyLine = loadImage("assemblyLine.png");
    //assigning strings (name of color) individually to boxes
    //the variable colors and boxImafea
  for (let i = 0; i < colors.length; i++) {
    let imgPath = 'box/box' + i + '.png';
    let boxImg = loadImage(imgPath);
    boxImages.push(boxImg);
  }
  //load backgrounds
  screenBackground = loadImage("background/screenBackground.jpg");
  screen1Background = loadImage("background/screen1Background.jpg");
  screen2Background = loadImage("background/screen2Background.gif");
  screen3Background = loadImage("background/screen3Background.gif");

  //load font
  myFont = loadFont('honk.ttf');

  //load sounds
  soundFormats('mp3', 'ogg');
  backgroundMusic = loadSound("workSong.mp3");
  audioFiles.push(loadSound("memo/Laura.mp3"));
  audioFiles.push(loadSound("memo/Lasse.mp3"));
  audioFiles.push(loadSound("memo/Olivia.mp3"));
  audioFiles.push(loadSound("memo/Signe.mp3"));
}


//---------------------------SETUP-------------------------
//setup canvas 
function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(40);
}

//----------------------------DRAW----------------------------
// Draw different screens 
function draw() {
    //creating conditional statements/if-statements, which check the value of the variable called screen.
    //if the screen is equal to 0, then it calls the screen0() function.
    //if the screen is equal to 1, then it calls the screen1() function.
  if (screen === 0) {
    screen0();
  } else if (screen === 1) {
    screen1();
  }

  backgroundMusic.onended(musicEnd);

  function musicEnd() {
  
    if (selectedCharacter === "PICK LASSE") {
      screen = 3;
      screen3();
    } else {
      screen = 2;
      screen2();
    }
  }
}


//--------------------------SCREEN 0------------------------
function screen0() {
  background(screenBackground);

  //text
  textFont(myFont);
  fill(255, 215, 0);
  stroke(255);
  strokeWeight(4);
  textSize(60);
  textAlign(CENTER);
  text('Who will make the most money?', width / 2, height / 5 - 60);

  textSize(30);
  text("Learn more? - press 'about' button", width / 2, height / 5 - 20);

// initaial gifs 
  push();
  imageMode(CENTER);
  image(lauraGif, windowWidth / 2 - 390, windowHeight / 2 + 80, 500, 500);
  image(lasseGif, windowWidth / 2 - 110, windowHeight / 2 + 80, 500, 500);
  image(oliGif, windowWidth / 2 + 160, windowHeight / 2 + 80, 500, 500);
  image(signeGif, windowWidth / 2 + 420, windowHeight / 2 + 80, 500, 500);
  pop();

  // all buttons
  if (!buttonsCreated) {
    createButtons();
    buttonsCreated = true;
  }
}
    //create pick buttons
    function createButtons() {
  let buttonX = windowWidth / 2 - 420;
  let buttonXAudioButtons = windowWidth / 2 - 420;
  let buttonY = windowHeight - 100;

  for (let i = 0; i < buttonsName.length; i++) {
    let button = createButton(buttonsName[i]);
    button.position(buttonX, buttonY);
    buttons.push(button);
    buttonX += 260;

    //remove pick buttons when mousePressed
    button.mousePressed(() => {
      screen = 1;
      removeAllButtons();
      backgroundMusic.play(); // Play the song
      selectedCharacter = buttonsName[i].split(".")[0];
      console.log("Background music started, screen is 1");
      console.log("Selected character:", selectedCharacter);
    });
  }
  // button sounds 
      //buttons kept drawing -therefore created in a different way
  //loop iterates through the audioFiles array, ensuring a button is created for each audio file.
  for (let i = 0; i < audioFiles.length; i++) {
    // Creates a button with the label from the buttonLabels array.
    let button = createButton(buttonLabels[i]); 
    //Assigns a function to the button that plays the corresponding audio file when clicked.
    button.mousePressed(() => playAudio(audioFiles[i]));
    //set pos
    button.position(buttonXAudioButtons, buttonY - 70);
    //buttonSounds.push(button);: Stores the button in the buttonSounds array for future reference or removal.
    buttonSounds.push(button);
    //Updates the X position for the next button
    buttonXAudioButtons += 260;
  }

  function playAudio(audio) {
    if (audio.isPlaying()) {
      audio.stop();
    }
    audio.play();
  }
}

//Remove buttons 
//creating a function called removeAllButton, which is removing the buttons, when they are pressed
function removeAllButtons() {
  for (let button of buttons) {
    //This line removes the current button element from the HTML document
    button.remove();
  }

  for (let button of buttonSounds) {
    button.remove();
  }

  /*This line sets the buttonsCreated variable to false, 
  indicating that no buttons are currently created on the screen. 
  It acts as a flag to control button creation in the future.*/
  buttonsCreated = false;  // Reset the button creation flag
}

//------------------------ SCREEN 1 -----------------------------
function screen1() {
  background(screen1Background);

 /*if (buttonsCreated) {
    removeAllButtons();  // Ensure buttons from screen 0 are removed
  }*/

  //Images of us holding boxes
  push();
  imageMode(CENTER);
  image(lauraBox, windowWidth / 2 - 370, windowHeight / 2 + 130, 500, 500);
  image(lasseBox, windowWidth / 2 - 120, windowHeight / 2 + 130, 500, 500);
  image(oliBox, windowWidth / 2 + 100, windowHeight / 2 + 130, 500, 500);
  image(signeBox, windowWidth / 2 + 350, windowHeight / 2 + 130, 500, 500);
  image(assemblyLine, windowWidth / 2, windowHeight / 2 + 250, windowWidth + 300, 500);
  pop();

  for (let i = 0; i < boxObjects.length; i++) {
    boxObjects[i].move();
    boxObjects[i].show();
  }

      // Initialize box objects
  for (let i = 0; i < min_BoxObjects; i++) {
    let organizedImage = boxImages[i % boxImages.length];
    let startX = 100 + i * 200;
    let startY = windowHeight / 2 + 210;
    if (boxObjects.length < min_BoxObjects) {
      let box = new Box(startX, startY, 100, 5, organizedImage);
      boxObjects.push(box);
    }
  }
}

//--------------------------SCREEN 2 -----------------------------
function screen2() {
  background(screen2Background);
}

//--------------------------SCREEN 3 ----------------------------- 
function screen3() {
  background(screen3Background);
}

  //RESIZE WINDOW
  function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
  }
