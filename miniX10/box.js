class Box {
  constructor(x, y, size, speed, image){
    this.pos = createVector(x, y);
    this.size = size;
    this.speed = speed;
    this.image = image;
  }


  //Making the boxes move until they hit the width of the window.
  move(){
    this.pos.x += this.speed;
    if (this.pos.x > width){
      this.pos.x -= width;
    }
  }

  show(){
    image(this.image, this.pos.x, this.pos.y, this.size, this.size);
  }

}

