let beamCounter = 0;
var throbberTitleArray = ["it's a new day...", "sunrise...", "seize the day...", "sunset..."];
var index = 0;

function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  frameRate(30); //how many times the frames are updated 
}

function draw(){
  // words underneath the sun
  fill(255);
  for (let i = 0; i < throbberTitleArray.length; i++) {
    text(throbberTitleArray[index], windowWidth / 2 - 30, windowHeight * 0.60);
    // iteration of the strings inside the array displayed on the screen
  }

  if (frameCount < 3) {
    background(0);
  }
  background(0, 10);

  // alpha is used for compressing the color of the sunbeams on the background

  if (frameCount % 1 == 0) {
    drawSunbeam(beamCounter)
    if (beamCounter < 40) {
    beamCounter += 0.5
    } else {
      beamCounter = 0
      index = (index + 1) % throbberTitleArray.length;
    }
    //control the animation of the sunbeam, 
    //incrementing its length until it reaches a certain threshold (40), 
    //then resetting it and advancing to the next text in the throbberTitleArray.
  }

  // sun
  let radius = 190; // defines the radius of the sun
  let startAngle = PI; // 180 degrees
  let endAngle = TWO_PI; // 360 degrees
  fill(255, 190, 20); // color of sun
  noStroke(0);
  arc(windowWidth / 2, windowHeight * 0.57, radius, radius, startAngle, endAngle, PIE); // creating half a circle
}


  function drawSunbeam(i) {
    push()
    translate(windowWidth / 2, windowHeight *0.57);
    let d = random(-2, -20) // (different)length(s) of sun beams - needs to get a random lenght between -2 and -20
    let j = 0.078; // the space between the sunbeams
    let x = (-300 + d) * cos(i*j); // x needs to start increasing slow and then fast
    let y = (-300 + d) * sin(i*j); // y need to start increasing fast and then slow
    stroke(235, 150, 5); // color of sunbeams
    strokeWeight(2);
    line(0, 0, x, y);
    pop()
  }