# Circle of Time

In the RunMe I have tried playing around with multiple things. First up I tried to play around with the little icon in the window-bar at the top. I thought it made sense to make it a sun so it followed the theme of the website. I also really enjoy playing around with the rgb to get the exact color that I want - it is a big opportunity for unfolding your creativity through colors.
The more advanced terms I used in this code were the for loop, the if statement, the 2D arc and the cos and sin. 
The for loop were for the words in the throbberTitleArray to appear on the screen at different times. I used the 2D arc to create the half circle that looked like a sun. 
Both the if statement and the cos and sin were for the sun beams. The if statements is for creating the animation of the sunbeams - it checks the frameCount is an integer value. This is for controlling that the action happens on every frame. The cos and sin are used for creating the round effect the beams have - they go from east to west (if you want to put it that way).

Please run the code [here](https://LauraAP.gitlab.io/aesthetic-programming/miniX03/)

In this code I have created a sun rising and going down again - an act from our daily lives. I got really inspired by the book The Techno-Galatic Guide to Software Observation where they talk about the difference between human time and computer time. So even though we know, that the sun is going up and down, in this code it actually rises and sets within the time of two seconds. That's pretty impossible in real life. But that's also how I see some things in computer-life. You can create the impossible.

The words underneath the sun also change as the sun rises again, but you are still able to see some "leftovers" from the previous word. This also is a symbol of how life moves fast but some things from yesterday might also be important today.

Another aspect from book I really liked were thinking more about the sundial time. How cool would it be if a computer ran on sundial time! Then as an example MacBooks would be different from which time-zone you were in. At least I think that would be quite unique (maybe also a bit annoying at times).

![](circleoftime.mov)
<br>
Please view the full repository [here](https://gitlab.com/lauraAP/aesthetic-programming/)
<br>

References
For this miniX03 I got help from:
<br>
[The Techno-Galatic Guide to Software Observation](https://monoskop.org/log/?p=20190)
<br>
[7.1 by Codingtrain](https://www.youtube.com/watch?v=VIQoUghHSxU&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=27)
<br>
[7.2 by Codingtrain](https://www.youtube.com/watch?v=RXWO3mFuW-I&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=28)
<br>
[Chapter 3, aesthetic programming](https://aesthetic-programming.net/pages/3-infinite-loops.html)
<br>
And I also got help from Chat.gpt for the for loop. I didn't copy paste but it helped me undestand what to do.